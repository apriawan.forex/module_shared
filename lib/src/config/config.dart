import 'package:flutter/material.dart';
import 'package:module_shared/src/config/flavor_type.dart';

class AppConfig {
  final String appName;
  final FlavorType flavor;
  AppConfig({required this.appName, required this.flavor});
}

class NavGlobalKey {
  static GlobalKey<NavigatorState> globalkey = GlobalKey<NavigatorState>();
}
