import 'webservice.constant.dart';

enum ModuleType {
  general,
  notification,
  leaveHO,
  izin,
  workflow,
  benefitMarriage,
  benefitGrief,
  benefitDisaster,
  benefitGreatLeave,
  heregistrasi,
}

class BspaceModule {
  static String? getRootUrl({required ModuleType moduleType}) {
    switch (moduleType) {
      case ModuleType.general:
        return WebServiceConstant.rootUrlGeneral;
      case ModuleType.notification:
        return WebServiceConstant.rootUrlNotification;
      case ModuleType.leaveHO:
        return WebServiceConstant.rootUrlLeaveHO;
      case ModuleType.izin:
        return WebServiceConstant.rootUrlIzin;
      case ModuleType.workflow:
        return WebServiceConstant.rootUrlWorkflow;
      case ModuleType.benefitMarriage:
        return WebServiceConstant.rootUrlBenefit;
      case ModuleType.benefitGrief:
        return WebServiceConstant.rootUrlBenefit;
      case ModuleType.benefitDisaster:
        return WebServiceConstant.rootUrlBenefit;
      case ModuleType.benefitGreatLeave:
        return WebServiceConstant.rootUrlBenefit;
      case ModuleType.heregistrasi:
        return WebServiceConstant.rootUrlHeregistrasi;
      default:
        return null;
    }
  }
}
