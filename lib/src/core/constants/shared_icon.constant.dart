import 'constant.dart';

class SharedIconConstant {
  static const String bumaLogo = 'assets/images/buma_logo.png';
  static const String bspaceLogo = 'assets/images/bspace_logo.png';
  static const String bgLogin = 'assets/images/illustration_login.png';
  static const String bgHeaderHris = 'assets/images/bg_header_home.png';
  static const String bgHeaderHome = 'assets/images/bg_header_home_new.png';
  static const String icDate = 'assets/icons/ic_date.png';
  static const String icNotification = 'assets/icons/ic_notification.png';
  static const String icNotificationEmpty = 'assets/icons/ic_notification_empty.png';
  static const String icApprovalEmpty = 'assets/icons/ic_approval_empty.png';
  static const String icLeave = 'assets/icons/ic_leave.png';
  static const String icAllData = 'assets/icons/ic_alldata.png';
  static const String icFinance = 'assets/icons/ic_finance.png';
  static const String icPscm = 'assets/icons/ic_pscm.png';
  static const String icDashboard = 'assets/icons/ic_dashboard.png';
  static const String icCuti = 'assets/icons/ic_cuti.png';
  static const String icTravel = 'assets/icons/ic_travel.png';
  static const String icTravelRequest = 'assets/icons/ic_travel_request.png';
  static const String icDeclaration = 'assets/icons/ic_declaration.png';
  static const String icIzin = 'assets/icons/ic_izin.png';
  static const String icLembur = 'assets/icons/ic_lembur.png';
  static const String icHeregist = 'assets/icons/ic_heregist.png';
  static const String icBantuan = 'assets/icons/ic_bantuan.png';
  static const String icBack = 'assets/icons/back.png';
  static const String icMyrequestEmpty = 'assets/icons/ic_myrequest_empty.png';
  static const String icMyrequestCuti = 'assets/icons/ic_myrequest_cuti.png';
  static const String icMyrequestFinance = 'assets/icons/ic_myrequest_finance.png';
  static const String icMyrequestIzin = 'assets/icons/ic_myrequest_izin.png';
  static const String icMyrequestPscm = 'assets/icons/ic_myrequest_pscm.png';
  static const String icMyrequestTravel = 'assets/icons/ic_myrequest_travel.png';
  static const String icMyrequestDeclaration = 'assets/icons/ic_myrequest_declaration.png';
  static const String icMyrequestHereg = 'assets/icons/ic_myrequest_hereg.png';
  static const String icMyrequestBenefit = 'assets/icons/ic_myrequest_benefit.png';
  static const String icMyrequestSpl = 'assets/icons/ic_myrequest_spl.png';
  static const String icDelete = 'assets/icons/ic_delete.png';
  static const String icFilter = 'assets/icons/ic_filter.png';
  static const String icFolder = 'assets/icons/ic_folder.png';
  static const String icGalery = 'assets/icons/ic_galeri.png';
  static const String icPicture = 'assets/icons/ic_picture.png';
  static const String icCutiTh1 = 'assets/icons/ic_cuti_tahunan_1.png';
  static const String icCutiTh2 = 'assets/icons/ic_cuti_tahunan_2.png';
  static const String icEdit = 'assets/icons/ic_edit.png';
  static const String icExchangeTicket = 'assets/icons/ic_exchange_ticket.png';
  static const String icChooseTicket = 'assets/icons/ic_choose_ticket.png';
  static const String icLeaveRequest = 'assets/icons/ic_cuti_request.png';
  static const String icLeaveTime = 'assets/icons/ic_cuti_request_time.png';
  static const String icRefundTicket = 'assets/icons/ic_refund_tiket.png';

  static const String icCaution = 'assets/icons/caution.png';
  static const String faqList = 'assets/json/faq.json';
  static const String countryList = 'assets/json/countrylist.json';
  static const String provinceList = 'assets/json/provincelist.json';
  static const String icPlane = 'assets/icons/ic_leave_site_plane.png';
  static const String icHotel = 'assets/icons/ic_leave_site_hotel.png';
  static const String icCar = 'assets/icons/ic_leave_site_car.png';
  static const String icTrain = 'assets/icons/ic_leave_site_train.jpeg';
  static const String icLocation = 'assets/icons/ic_location_grey.png';
  static const String icPlaneGrey = 'assets/icons/ic_plane_grey.png';
  static const String icEmptyFlight = 'assets/icons/ic_empty_flight.png';
  static const String icEmptyHotel = 'assets/icons/ic_empty_hotel.png';
  static const String icEmptyCar = 'assets/icons/ic_empty_car.png';
  static const String icEmptyRefundTicket = 'assets/icons/ic_empty_refund.png';
  static const String icEmptyAddTicket = 'assets/icons/ic_empty_add_ticket.png';
  static const String icEmptySpl = 'assets/icons/ic_empty_spl.png';
  static const String icEmptyDeclaration = 'assets/icons/ic_empty_declaration.png';

  static const String icDateGrey = 'assets/icons/ic_date_grey.png';
  static const String icPlafon = 'assets/icons/ic_plafon.png';
  static const String icArrow = 'assets/icons/ic_arrow_grey.png';
  static const String icTicket = 'assets/icons/ic_ticket.png';
  static const String icRefund = 'assets/icons/ic_refund.png';

//spl
  static const String icSunny = 'assets/icons/ic_sunny.png';
  static const String icMoon = 'assets/icons/ic_moon.png';
  static const String icSplLeave = 'assets/icons/ic_spl_leave.png';

//benefit
  static const String icMarriage = 'assets/icons/ic_marriage.png';
  static const String icGrief = 'assets/icons/ic_grief.png';
  static const String icDisaster = 'assets/icons/ic_disaster.png';
  static const String icLeaveCompensation = 'assets/icons/ic_leave_compensation.png';
  static const String icMarriageColor = 'assets/icons/ic_marriage_color.png';
  static const String icGriefColor = 'assets/icons/ic_grief_color.png';
  static const String icDisasterColor = 'assets/icons/ic_disaster_color.png';
  static const String icLeaveCompensationColor = 'assets/icons/ic_leave_compensation_color.png';

  static const String travelokaLogo = 'assets/images/traveloka_logo.png';

//performance
  static const String icIpd = 'assets/icons/performance/ic_ipd.png';
  static const String icPip = 'assets/icons/performance/ic_pip.png';
  static const String icProbation = 'assets/icons/performance/ic_probation.png';
  static const String icCoaching = 'assets/icons/performance/ic_coaching.png';
  static const String icFeedback = 'assets/icons/performance/ic_feedback.png';
  static const String icCultureBehavior = 'assets/icons/performance/ic_culture_behavior.png';

  static const String icNavHomeAct = 'assets/icons/ic_navhome_active.png';
  static const String icNavHomeDeact = 'assets/icons/ic_navhome_disable.png';
  static const String icMyReqAct = 'assets/icons/ic_myrequest_active.png';
  static const String icMyReqDeact = 'assets/icons/ic_myrequest_disable.png';
  static const String icApprovalAct = 'assets/icons/ic_approval_active.png';
  static const String icApprovalDeact = 'assets/icons/ic_approval_disable.png';
  static const String icProfileAct = 'assets/icons/ic_profile_active.png';
  static const String icProfileDeact = 'assets/icons/ic_profile_disable.png';

  //others
  static const String icUnderContstruction = 'assets/icons/ic_underconstruction.png';
  static const String icPdf = 'assets/icons/ic_pdf.png';

  static const Map<String, String> moduleTypeIconMap = {
    ModuleTypeConstant.leaveHO: icMyrequestCuti,
    ModuleTypeConstant.izin: icMyrequestIzin,
    ModuleTypeConstant.heregistrasi: icMyrequestHereg,
    ModuleTypeConstant.travel: icMyrequestTravel,
    ModuleTypeConstant.benefitGreatLeave: icMyrequestBenefit,
    ModuleTypeConstant.benefitDisaster: icMyrequestBenefit,
    ModuleTypeConstant.benefitGrief: icMyrequestBenefit,
    ModuleTypeConstant.benefitMarriage: icMyrequestBenefit,
    ModuleTypeConstant.declaration: icMyrequestDeclaration,
    ModuleTypeConstant.spl: icMyrequestSpl,
    ModuleTypeConstant.pip: icPip,
    ModuleTypeConstant.ipd: icIpd,
    ModuleTypeConstant.coaching: icCoaching,
  };

  static String getTaskIconMap({required String type}) {
    final icon = moduleTypeIconMap[type];
    if (icon == null) {
      return icMyrequestFinance;
    } else {
      return icon;
    }
  }
}
