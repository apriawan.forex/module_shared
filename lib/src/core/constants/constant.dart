class ConfigConstant {
  static const domain = 'BUKITMAKMUR';
  static const String dateformat = "dd MMM yyyy";
  static const String dateFormatYMD = 'yyyy-MM-dd';
  static const String timeformat = "HH:mm";
}

class UploadConstant {
  static const int maxSizeDefault = 2;
  static const List<String> fileTypeDefault = ['pdf', 'jpg', 'jpeg', 'png'];
  static const String fileTypeTextDefault = 'pdf, jpg, jpeg, atau png';
  static const List<String> fileTypeImage = ['jpg', 'jpeg', 'png'];
  static const String fileTypeTextImage = 'jpg, jpeg, atau png';
}

class LeaveConstant {
  static const annualLeave = 3;
  static const greatLeave = 2;
}

class BenefitAttachmentID {
  static const fileKK = 1;
  static const fileGriefNote = 2;
  static const fileKKHusband = 3;
  static const fileKKWife = 4;
  static const fileKKNew = 5;
  static const fileMarriageLetter = 6;
  static const documentLetterRTRW = 7;
  static const housePhoto = 8;
}

class WorkflowIdConstant {
  static const int leaveHO = 100;
  static const int travel = 200;
  static const int declaration = 300;
  static const int leaveCancellation = 400;
  static const int additionalTicketLeave = 500;
  static const int additionalTicketTravel = 2300;

  static const int leaveSite = 600;
  static const int refund = 700;
  static const int izin = 800;
  static const int heregistrasi = 900;
  static const int benefitGreatLeave = 1200;
  static const int benefitDisaster = 1300;
  static const int benefitGrief = 1400;
  static const int benefitMarriage = 1500;
  static const int refundTravel = 2400;
  static const int spl = 1800;
}

class TaskStatusIdConstant {
  static const int waiting = 30;
  static const int completed = 50;
  static const int error = 60;
}

class TaskIdCancelLeaveRequestConstant {
  static const int cancelComplete = 406;
  static const int cancelReject = 408;
}

class TaskIdCancelTravelRequestConstant {
  static const int cancelComplete = 406;
  static const int cancelReject = 403;
}

class TaskIdLeaveRefundConstant {
  static const int approvalBySpv = 702;
  static const int rejectByApprover = 705;
  static const int rejectByGs = 710;
}

class TaskIdTravelRefundConstant {
  static const int approvalBySpv = 2402;
  static const int rejectByApprover = 2405;
  static const int rejectByGs = 2409;
  static const int rejectByGs2 = 2410;
}

class TaskTypeIdConstant {
  static const int form = 1;
  static const int decision = 2;
  static const int condition = 3;
  static const int operation = 4;
  static const int approved = 5;
  static const int rejected = 6;
  static const int sap = 7;
}

class TaskIdTravelConstant {
  static const draft = 201;
  static const approval = 202;
  static const rejected = 203;
  static const revise = 204;
  static const transport = 208;
  static const downPayment = 210;
  static const transportReservation = 212;
  static const transportComplete = 214;
  static const reservationByGS = 218;
  static const approvalCancel = 402;
  static const rejectedByGS = 220;
  static const rejectedCancel = 403;
}

class TaskIdLeaveSiteConstant {
  static const draft = 601;
  static const approvalSpv = 602;
  static const rejectedSpv = 603;
  static const revise = 604;
  static const reservationGS = 612;
  static const complete = 620;
  static const rejectedByGS = 626;
  static const reservationTicket = 632;
}

class TaskResultIdConstant {
  static const int accept = 1;
  static const int submit = 2;
  static const int approve = 3;
  static const int revise = 4;
  static const int reject = 5;
  static const int cancel = 6;
  static const int cancelSubmit = 7;
  static const int cancelApprove = 8;
  static const int cancelReject = 9;
  static const int siteSubmit = 10;
  static const int leaveSiteApprove = 11;
  static const int leaveSiteApproveGs = 12;
  static const int leaveSiteReject = 13;
  static const int leaveSiteRevise = 14;
  static const int leaveSiteRejectGs = 15;
  static const int refundRevise = 16;
  static const int refundSubmit = 17;
  static const int refundReject = 18;
  static const int refundApprove = 19;
  static const int cancelSiteReject = 22;
  static const int addTicketApproveGs = 23;
  static const int addTicketRejectGs = 24;
  static const int heregistrasiReject = 25;
  static const int heregistrasiRevise = 26;
  static const int refundApproveGs = 27;
  static const int refundRejectGs = 28;
  static const int cancelSiteApproved = 29;
}

class GenderID {
  static const pria = 1;
  static const wanita = 2;
}

class StatusRequest {
  static const submit = 'Submit';
  static const revise = 'Revise';
  static const draft = 'Draft';
  static const resubmit = 'Resubmit';
  static const reject = 'Reject';
  static const deleteDraft = 'Yes';
  static const approve = 'Approve';
}

class TaskStatus {
  static const waiting = 'Waiting';
  static const onProgress = 'On Progress';
  static const completed = 'Completed';
  static const needAction = 'Need Action';
  static const assignment = 'Assignment';
  static const draft = 'Draft';
  static const rejected = 'Rejected';
  static const cancelled = 'Cancelled';
}

class LocationIdConstant {
  static const List<String> headOffice = ["2000", "3000", "2090"];
}

class TaskIdTicketConstant {
  static const reservationByGsLeave = 506;
  static const reservationByGsDinas = 2306;
  static const completedLeave = 507;
  static const completedDinas = 2307;
  static const completedLeaveReject = 509;
  static const completedDinasReject = 2309;
}

class TaskIdIzinConstant {
  static const approvalSpv = 802;
  static const revise = 804;
  static const approvalHR = 809;
  static const uploadFile = 811;
}

class ModuleTypeConstant {
  static const leaveHO = 'LVR';
  static const izin = 'IZN';
  static const heregistrasi = 'HRG';
  static const travel = 'TRA';
  static const benefitGrief = 'DKA';
  static const benefitMarriage = 'PNK';
  static const benefitGreatLeave = 'KCB';
  static const benefitDisaster = 'MSB';
  static const declaration = 'DCL';
  static const spl = 'SPL';
  static const coaching = 'COC';
  static const ipd = 'IPD';
  static const pip = 'PIP';
}

class HeregistIdKeluarga {
  static const int pasangan = 1;
  static const int anak = 2;
  static const int bapak = 3;
  static const int ibu = 4;
  static const int saudaraPria = 5;
  static const int saudaraWanita = 6;
  static const int mertuaPria = 7;
  static const int mertuaWanita = 8;
}

class HeregistIdPendidikan {
  static const String sd = 'Z0';
  static const String smp = 'Z1';
  static const String sma = 'Z2';
  static const String diploma = 'Z3';
  static const String s1 = 'Z4';
  static const String s2 = 'Z5';
  static const String s3 = 'Z6';
  static const String externalCourse = 'Z7';
}

class HeregistIdAttachment {
  static const int ktp = 1;
  static const int npwp = 2;
  static const int bpjs = 3;
  static const int bpjsKetenagakerjaan = 4;
  static const int kk = 5;
  static const int bukuNikah = 6;
  static const int ijazah = 7;
  static const int suratKerja = 8;
  static const int rekening = 9;
}

class HeregistReligion {
  static const String islam = '01';
  static const String christian = '02';
  static const String catholic = '03';
  static const String hindu = '04';
  static const String buddha = '05';
  static const String confucianism = '06';
}
