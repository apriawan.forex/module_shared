class TableConstant {
  static const String tbMProfile = 'tb_m_profile';
  static const String tbMMenu = 'tb_m_menu';
}

class ProfileKeyConstant {
  static const String keyTheme = 'theme';
  static const String keyTranslation = 'translation';
  static const String keyTokenGeneral = 'token.general';
  static const String keyApimToken = 'apim.token';
  static const String keyIpPublic = 'ip.public';
  static const String keyFcmToken = 'fcm.token';
  static const String keyEmployeeInfo = 'employee.employeeInfo';
}

class MenuKeyConstant {
  static const String keyMenuInfo = 'menu.menuInfo';
}
