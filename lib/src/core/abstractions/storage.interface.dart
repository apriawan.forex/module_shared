abstract class IStorage {
  Future<void> write({required String key, required String? value});
  Future<String?> read(String key);
  Future<void> remove({required String key});
  Future<void> erase();
  Future<Map<dynamic, dynamic>?> getAllData();
}
