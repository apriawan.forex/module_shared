import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:module_shared/module_shared.dart';
import 'package:module_shared/src/config/flavor_type.dart';
import 'package:module_shared/src/translations/shared.translate.dart';
import 'package:open_filex/open_filex.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

class SharedWidget {
  static Widget banner(AppConfig config, Widget child) {
    return config.flavor != FlavorType.prd
        ? Banner(
            location: BannerLocation.topEnd,
            message: config.flavor == FlavorType.uat ? 'UAT' : 'DEV',
            color: config.flavor == FlavorType.uat ? Colors.yellow.shade900 : Colors.red,
            child: child,
          )
        : SizedBox(child: child);
  }

  static Widget btnWidget({
    required label,
    required VoidCallback onPressed,
    Color? color,
    Color? fontColor,
    TextStyle? textStyle,
    double? minimumSize,
    double? padding,
    double? radius,
    FontWeight? fontWeight = FontWeight.bold,
    double? fontSize,
    Widget? icon,
  }) {
    ElevatedButton button;
    ButtonStyle buttonStyle = ElevatedButton.styleFrom(
        backgroundColor: (color == null) ? null : color,
        minimumSize: (minimumSize == null) ? null : Size.fromHeight(minimumSize),
        padding: (padding == null) ? null : EdgeInsets.symmetric(horizontal: padding, vertical: padding),
        shape: (radius == null)
            ? null
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(radius),
              ));
    TextStyle textstyle = TextStyle(
        color: (fontColor == null) ? null : fontColor,
        fontSize: (fontSize == null) ? null : fontSize,
        fontWeight: (fontWeight == null) ? null : fontWeight);
    (icon == null)
        ? button = ElevatedButton(style: buttonStyle, onPressed: onPressed, child: Text(label, style: textstyle))
        : button = ElevatedButton.icon(style: buttonStyle, onPressed: onPressed, icon: icon, label: Text(label, style: textstyle));
    return button;
  }

  static Widget btnOutlinedWidget({
    required label,
    required VoidCallback onPressed,
    Color? color,
    Color? fontColor,
    TextStyle? textStyle,
    double? minimumSize,
    double? padding,
    double? radius,
    FontWeight? fontWeight = FontWeight.bold,
    double? fontSize,
    Widget? icon,
  }) {
    OutlinedButton button;
    ButtonStyle buttonStyle = OutlinedButton.styleFrom(
        minimumSize: (minimumSize == null) ? null : Size.fromHeight(minimumSize),
        padding: (padding == null) ? null : EdgeInsets.symmetric(horizontal: padding, vertical: padding),
        side: (color == null) ? null : BorderSide(color: color),
        shape: (radius == null)
            ? null
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(radius),
              ));

    TextStyle textstyle = TextStyle(
        color: (fontColor == null) ? null : fontColor,
        fontSize: (fontSize == null) ? null : fontSize,
        fontWeight: (fontWeight == null) ? null : fontWeight);
    (icon == null)
        ? button = OutlinedButton(style: buttonStyle, onPressed: onPressed, child: Text(label, style: textstyle))
        : button = OutlinedButton.icon(style: buttonStyle, onPressed: onPressed, icon: icon, label: Text(label, style: textstyle));
    return button;
  }

  static Future<bool?> msgToast(String msg) {
    return Fluttertoast.showToast(msg: msg, toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.BOTTOM, timeInSecForIosWeb: 2);
  }

  static Widget btnTextWidget({
    required label,
    required VoidCallback onPressed,
    Color? color,
    Color? fontColor,
    TextStyle? textStyle,
    double? minimumSize,
    double? padding,
    double? radius,
    FontWeight? fontWeight = FontWeight.bold,
    double? fontSize,
    Widget? icon,
  }) {
    TextButton button;
    ButtonStyle buttonStyle = TextButton.styleFrom(
        minimumSize: (minimumSize == null) ? null : Size.fromHeight(minimumSize),
        padding: (padding == null) ? null : EdgeInsets.symmetric(horizontal: padding, vertical: padding),
        shape: (radius == null)
            ? null
            : RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(radius),
              ));
    TextStyle textstyle = TextStyle(
        color: (fontColor == null) ? null : fontColor,
        fontSize: (fontSize == null) ? null : fontSize,
        fontWeight: (fontWeight == null) ? null : fontWeight);
    (icon == null)
        ? button = TextButton(style: buttonStyle, onPressed: onPressed, child: Text(label, style: textstyle))
        : button = TextButton.icon(style: buttonStyle, onPressed: onPressed, icon: icon, label: Text(label, style: textstyle));
    return button;
  }

  static Future<bool?> msgDialog({
    required title,
    required desc,
    required alerttype,
    required BuildContext context,
    btntitleleft,
    btntitleright,
    onpressleft,
    onpressright,
  }) {
    AlertType alerticon = AlertType.none;
    if (alerttype == 'error') {
      alerticon = AlertType.error;
    }
    if (alerttype == 'info') {
      alerticon = AlertType.info;
    }
    if (alerttype == 'none') {
      alerticon = AlertType.none;
    }
    if (alerttype == 'success') {
      alerticon = AlertType.success;
    }
    if (alerttype == 'warning') {
      alerticon = AlertType.warning;
    }
    var alertStyle = AlertStyle(
      animationType: AnimationType.grow,
      isCloseButton: false,
      isOverlayTapDismiss: false,
      descStyle: (Theme.of(context).brightness == Brightness.dark) ? const TextStyle(color: Colors.white) : const TextStyle(color: Colors.black),
      animationDuration: const Duration(milliseconds: 400),
      alertBorder: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(0.0),
        side: const BorderSide(
          color: Colors.grey,
        ),
      ),
      titleStyle: TextStyle(
        color: ColorTheme.maincolor,
      ),
    );
    return Alert(
      onWillPopActive: true,
      context: context,
      type: alerticon,
      style: alertStyle,
      title: title,
      desc: desc,
      buttons: [
        if (btntitleleft != null)
          DialogButton(
            onPressed: onpressleft,
            color: const Color.fromRGBO(0, 179, 134, 1.0),
            child: Text(
              btntitleleft,
              style: const TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
        if (btntitleright != null)
          DialogButton(
            onPressed: onpressright,
            color: Colors.red,
            child: Text(
              btntitleright,
              style: const TextStyle(color: Colors.white, fontSize: 20),
            ),
          )
      ],
    ).show();
  }

  static Widget label({
    required String text,
    required BuildContext context,
    FontWeight? fontWeight,
    double? fontSize,
    Color? color,
    double opacity = 1,
    TextOverflow? textOverflow,
    bool? softWrap,
    TextAlign? textAlign,
    TextStyle? textStyle,
  }) {
    textStyle ??= TextThemeC.poppins.bodyMedium;
    return Text(
      text,
      textAlign: textAlign,
      overflow: textOverflow,
      softWrap: softWrap,
      style: textStyle!.merge(TextStyle(color: (color != null) ? color.withOpacity(opacity) : null, fontSize: fontSize, fontWeight: fontWeight)),
    );
  }

  static Widget errorTextRetry({required String error, required VoidCallback onPressed, Color? fontColor}) {
    return SingleChildScrollView(
        child: btnTextWidget(label: '$error. Tap to retry', onPressed: onPressed, fontColor: fontColor, fontWeight: FontWeight.normal));
  }

  static Future<dynamic> modalBottomWidget({
    required BuildContext context,
    required Widget widget,
  }) async {
    dynamic returnData;
    await showModalBottomSheet(
        isScrollControlled: true,
        isDismissible: true,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadiusDirectional.only(
            topEnd: Radius.circular(25),
            topStart: Radius.circular(25),
          ),
        ),
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
            child: widget,
          );
        }).then((value) {
      returnData = value;
    });
    return returnData;
  }

  static Future<Map<String, dynamic>> bottomUploadFile(BuildContext context) async {
    Map<String, dynamic> data = {};
    await modalBottomWidget(
        context: context,
        widget: DraggableScrollableSheet(
          expand: false,
          initialChildSize: 0.15,
          minChildSize: 0.15,
          builder: (context, scrollController) {
            return Column(
              children: [
                buildHandle(context),
                Expanded(
                    child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: [
                          IconButton(
                            icon: Image.asset(
                              SharedIconConstant.icFolder,
                            ),
                            onPressed: () async {
                              FilePickerResult? file = await FilePicker.platform.pickFiles(
                                type: FileType.custom,
                                allowedExtensions: UploadConstant.fileTypeDefault,
                                allowCompression: true,
                              );
                              if (file != null) {
                                PlatformFile platformFile = file.files.first;
                                data['name'] = platformFile.name;
                                data['path'] = platformFile.path;
                                data['size'] = platformFile.size;
                                data['extension'] = platformFile.extension;
                                if (context.mounted) Navigator.pop(context, data);
                              }
                            },
                          ),
                          Text(
                            SharedTranslate.folder,
                            softWrap: true,
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          IconButton(
                            icon: Image.asset(
                              SharedIconConstant.icGalery,
                            ),
                            onPressed: () async {
                              FilePickerResult? file = await FilePicker.platform.pickFiles(
                                type: FileType.custom,
                                allowedExtensions: UploadConstant.fileTypeImage,
                                allowCompression: true,
                              );
                              if (file != null) {
                                PlatformFile platformFile = file.files.first;
                                data['name'] = platformFile.name;
                                data['path'] = platformFile.path;
                                data['size'] = platformFile.size;
                                data['extension'] = platformFile.extension;
                                if (context.mounted) Navigator.pop(context, data);
                              }
                            },
                          ),
                          Text(
                            SharedTranslate.gallery,
                            softWrap: true,
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          IconButton(
                            icon: Image.asset(
                              SharedIconConstant.icPicture,
                            ),
                            onPressed: () async {
                              ImagePicker picker = ImagePicker();
                              final XFile? file = await picker.pickImage(source: ImageSource.camera, imageQuality: 50);
                              if (file != null) {
                                data['name'] = file.name;
                                data['path'] = file.path;
                                data['size'] = File(file.path).readAsBytesSync().lengthInBytes;
                                data['extension'] = file.path.split('.').last;
                                if (context.mounted) Navigator.pop(context, data);
                              }
                            },
                          ),
                          Text(
                            SharedTranslate.camera,
                            softWrap: true,
                          )
                        ],
                      ),
                    ),
                  ],
                )),
              ],
            );
          },
        ));
    return data;
  }

  static void showPdf({required BuildContext context, required String path}) {
    bool validURL = Uri.parse(path).isAbsolute;
    if (validURL) {
      launchUrl(Uri.parse(path));
    } else {
      OpenFilex.open(path);
    }
  }

  static void showImage({required BuildContext context, required String path}) {
    bool validURL = Uri.parse(path).isAbsolute;
    showDialog(
      context: context,
      builder: (_) => Dialog(
          child: SizedBox(
        child: (validURL)
            ? Image.network(
                path,
                fit: BoxFit.cover,
              )
            : Image.file(
                File(path),
                fit: BoxFit.cover,
              ),
      )),
    );
  }

  static Widget buildHandle(BuildContext context) {
    final theme = Theme.of(context);

    return FractionallySizedBox(
      widthFactor: 0.25,
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 12.0,
        ),
        child: Container(
          height: 5.0,
          decoration: BoxDecoration(
            color: theme.dividerColor,
            borderRadius: const BorderRadius.all(Radius.circular(2.5)),
          ),
        ),
      ),
    );
  }

  static Widget txtFormLabel(
      {required id,
      required String label,
      double? fontSize,
      String? hintText,
      TextInputType? keyboardType = TextInputType.text,
      TextInputAction? textInputAction,
      bool? readOnly,
      bool? enabled,
      int? maxLines,
      int? maxLength,
      bool? obsecure,
      String? msgValidator,
      GestureDetector? icon,
      GestureTapCallback? onTap,
      GestureDetector? suffixIcon,
      ValueChanged<String>? onChanged,
      ValueChanged<String>? onSubmitted,
      List<TextInputFormatter>? inputFormatters}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 10,
        ),
        Text(
          label,
          style: TextStyle(color: (enabled ?? true) ? ColorTheme.maincolor : Colors.grey),
        ),
        TextFormField(
            controller: id,
            keyboardType: keyboardType,
            textInputAction: textInputAction,
            readOnly: readOnly ?? false,
            enabled: enabled,
            maxLines: maxLines,
            maxLength: maxLength,
            onTap: onTap,
            onChanged: onChanged,
            onFieldSubmitted: onSubmitted,
            inputFormatters: inputFormatters,
            style: TextStyle(
              fontSize: fontSize,
            ),
            decoration: InputDecoration(icon: icon, hintText: hintText, hintStyle: TextStyle(fontSize: fontSize), suffixIcon: suffixIcon),
            obscureText: obsecure ?? false,
            validator: (value) {
              if ((value!.isEmpty) && msgValidator != null) {
                return msgValidator;
              }
              return null;
            })
      ],
    );
  }

  static Widget txtDropdown({
    required String label,
    required List listItem,
    required String valueName,
    required String descName,
    required BuildContext context,
    required onChanged,
    onTap,
    dynamic currentValue,
    bool? enabled,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(
          height: 10,
        ),
        Text(
          label,
          style: TextStyle(color: (enabled ?? true) ? ColorTheme.maincolor : Colors.grey),
        ),
        DropdownButtonHideUnderline(
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: DropdownButton(
                isExpanded: true,
                icon: const Icon(Icons.arrow_drop_down),
                elevation: 16,
                value: (currentValue == '' || currentValue == null) ? listItem.first[valueName] : currentValue,
                items: listItem.map((map) {
                  return DropdownMenuItem(
                    value: map[valueName],
                    child: Text(map[descName]),
                  );
                }).toList(),
                onChanged: (enabled ?? true) ? onChanged : null,
                onTap: onTap,
              ),
            ),
          ),
        ),
      ],
    );
  }

  static Widget txtSearchList(
      {required TextEditingController txtid,
      required ValueChanged<String>? onChanged,
      required ValueChanged<String>? onSubmitted,
      required GestureDetector? suffixIcon}) {
    return TextField(
      onChanged: onChanged,
      onSubmitted: onSubmitted,
      controller: txtid,
      decoration: InputDecoration(
          hintText: "Search...",
          prefixIcon: const Icon(
            Icons.search,
            size: 20,
          ),
          filled: true,
          contentPadding: const EdgeInsets.all(8),
          suffixIcon: suffixIcon),
    );
  }

  static Future<String> showCalendar({
    required BuildContext context,
    required TextEditingController id,
    DateTime? firstDate,
    DateTime? lastDate,
  }) {
    return showDatePicker(
            context: context,
            initialDate: DateFormat(ConfigConstant.dateformat).parse(id.text),
            firstDate: firstDate ?? DateTime(2010),
            lastDate: lastDate ?? DateTime(9999))
        .then((value) {
      if (value != null) {
        return id.text = DateFormat(ConfigConstant.dateformat).format(value);
      } else {
        return id.text = id.text;
      }
    });
  }

  static Future<String> showTime({
    required BuildContext context,
    required TextEditingController id,
  }) {
    return showTimePicker(context: context, initialTime: TimeOfDay(hour: int.parse(id.text.split(":")[0]), minute: int.parse(id.text.split(":")[1])))
        .then((value) {
      if (value != null) {
        return id.text = '${value.hour}:${value.minute}';
      } else {
        return id.text = id.text;
      }
    });
  }

  static Widget tabBar({required controllerTab, required BuildContext context, required List<Tab> tabs}) {
    return TabBar(
      controller: controllerTab,
      unselectedLabelColor: ColorTheme.maincolor,
      labelColor: Colors.white,
      labelPadding: const EdgeInsets.symmetric(horizontal: 5),
      labelStyle: TextThemeC.poppins.bodyMedium!.copyWith(fontWeight: FontWeight.bold),
      indicator: BoxDecoration(borderRadius: BorderRadius.circular(10.0), color: ColorTheme.maincolor),
      tabs: tabs,
    );
  }

  static Widget rate({required ValueChanged<double> onRatingUpdate}) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        RatingBar.builder(
            initialRating: 0,
            minRating: 0,
            direction: Axis.horizontal,
            allowHalfRating: false,
            itemCount: 5,
            itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
            itemBuilder: (context, _) => Icon(Icons.star, color: ColorTheme.maincolor),
            onRatingUpdate: onRatingUpdate),
      ],
    );
  }

  static Widget chip({required String label, VoidCallback? onDeleted}) {
    return Chip(
      label: Text(
        label,
      ),
      onDeleted: onDeleted,
      deleteIconColor: Colors.red,
      padding: const EdgeInsets.all(8),
    );
  }
}
