import 'package:flutter/material.dart';
import 'appbar.theme.dart';
import 'button.theme.dart';
import 'colorsScheme.theme.dart';
import 'inputdecoration.theme.dart';
import 'text.theme.dart';

class Themes {
  static final light = ThemeData.light().copyWith(
    colorScheme: ColorSchemeTheme.colorScheme,
    appBarTheme: AppBarThemeC.appBar,
    textTheme: TextThemeC.poppins.apply(bodyColor: Colors.black),
    floatingActionButtonTheme: ButtonThemeC.floatingButtonThemeData,
    elevatedButtonTheme: ElevatedButtonThemeData(style: ButtonThemeC.elevatedButtonStyle),
    outlinedButtonTheme: OutlinedButtonThemeData(style: ButtonThemeC.outlinedButtonStyle),
    textButtonTheme: TextButtonThemeData(style: ButtonThemeC.textButtonStyle),
    inputDecorationTheme: InputDecorationThemeC.inputDecorationTheme,
  );

  static final dark = ThemeData.dark().copyWith(
    colorScheme: ColorSchemeTheme.colorSchemeDark,
    appBarTheme: AppBarThemeC.appBarDark,
    textTheme: TextThemeC.poppins.apply(bodyColor: Colors.white),
    floatingActionButtonTheme: ButtonThemeC.floatingButtonThemeDataDark,
    elevatedButtonTheme: ElevatedButtonThemeData(style: ButtonThemeC.elevatedButtonStyleDark),
    outlinedButtonTheme: OutlinedButtonThemeData(style: ButtonThemeC.outlinedButtonStyleDark),
    textButtonTheme: TextButtonThemeData(style: ButtonThemeC.textButtonStyleDark),
    inputDecorationTheme: InputDecorationThemeC.inputDecorationThemeDark,
  );
}
