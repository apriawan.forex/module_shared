import 'package:flutter/material.dart';

import 'color.theme.dart';

class ColorSchemeTheme {
  static ColorScheme colorScheme = ColorScheme.fromSeed(
    brightness: Brightness.light,
    seedColor: ColorTheme.primary500,
  );

  static ColorScheme colorSchemeDark = ColorScheme.fromSeed(
    brightness: Brightness.dark,
    seedColor: ColorTheme.maincolorDark,
  );
}
