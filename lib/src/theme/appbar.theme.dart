import 'package:flutter/material.dart';

import 'color.theme.dart';

class AppBarThemeC {
  static AppBarTheme appBar = AppBarTheme(
    color: ColorTheme.primary500,
    foregroundColor: Colors.white,
    titleTextStyle: const TextStyle(fontSize: 24),
  );
  static AppBarTheme appBarDark = AppBarTheme(
    color: ColorTheme.maincolorDark,
    foregroundColor: Colors.white,
    titleTextStyle: const TextStyle(fontSize: 24),
  );
}
