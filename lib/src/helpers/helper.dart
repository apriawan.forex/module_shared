import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:module_shared/src/core/constants/constant.dart';
import 'package:module_shared/src/model/file_upload.dart';

class Helper {
  static Future<Map<String, dynamic>> parseJsonFromAssets(String assetsPath) async {
    return rootBundle.loadString(assetsPath).then((jsonStr) => jsonDecode(jsonStr) as Map<String, dynamic>);
  }

  static Future<List> parseJsonListFromAssets(String assetsPath) async {
    return rootBundle.loadString(assetsPath).then((jsonStr) => jsonDecode(jsonStr) as List);
  }

  static int diffDays({required DateTime startDate, required DateTime endDate}) {
    return endDate.difference(startDate).inDays + 1;
  }

  static String convToDate(String date) {
    DateTime dateTime = DateFormat(ConfigConstant.dateformat).parse(date, true);
    return DateFormat(ConfigConstant.dateFormatYMD).format(dateTime.toUtc());
  }

  static String convDateStringToFormat(String dateString) {
    if (dateString.isEmpty) {
      return '';
    } else {
      return DateFormat(ConfigConstant.dateformat).format(DateTime.parse(dateString)).toString();
    }
  }

  static double getSizeFile(int size) {
    try {
      return double.parse(((size / 1024) / 1024).toStringAsFixed(2));
    } catch (e) {
      return 0;
    }
  }

  static bool absoluteFile(FileUpload fileUpload) {
    if (Uri.parse(fileUpload.file!).isAbsolute) {
      return true;
    } else {
      return false;
    }
  }

  // static Config getConfig() {
  //   return Config(
  //     tenant: '${ConfigEnvironments.getEnvironments()[EnvConstant.aadTenantId]}',
  //     clientId: '${ConfigEnvironments.getEnvironments()[EnvConstant.aadClientId]}',
  //     redirectUri: '${ConfigEnvironments.getEnvironments()[EnvConstant.aadRedirectUrl]}',
  //     scope: 'openid profile offline_access ${ConfigEnvironments.getEnvironments()[EnvConstant.scopeGeneral]}',
  //     navigatorKey: navigatorKey,
  //   );
  // }
}
